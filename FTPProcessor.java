import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.apache.commons.net.ftp.*;
import org.apache.commons.net.io.CopyStreamEvent;
import org.apache.commons.net.io.CopyStreamListener;

import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Connects to Rovi ftp site, looks for a .zip files that hasn't been processed yet (not in "ranfiles")
 * Downloads the .zip file from Rovi
 * Processes the newly downloaded .zip file, uploading contents to the specified AS3 bucket/folder
 */
public class FTPProcessor {
    public static void main(String[] args) {
        System.out.println("Starting FTPProcessor on "+new Date());
        long startTime = System.currentTimeMillis();
        FTPClient ftp = new FTPClient();
        try {
            // load args and parameters
            if (args.length < 2) {
                System.out.println("Usage: java FTPProcessor bucket folder [update [quiet]] ");
                System.exit(1);
            }

            ClientConfiguration s3conf = new ClientConfiguration();
            //System.out.println("timeout:"+s3conf.getSocketTimeout());
            s3conf.setSocketTimeout(100000);
            //System.out.println("new timeout:"+s3conf.getSocketTimeout());
            AmazonS3 s3 = new AmazonS3Client(new PropertiesCredentials(
                    FTPProcessor.class.getResourceAsStream("AwsCredentials.properties")), s3conf);

            Properties ftpProps = new Properties();
            ftpProps.load(FTPProcessor.class.getResourceAsStream("ftpRovi.properties"));
            if (ftpProps.getProperty("server") == null) {
                System.out.println("FTP server info not found in ftpRovi.properties");
                System.exit(1);
            }

            String bucketName = args[0];
            String folder = (args.length > 1) ? args[1] : "";
            // update means don't need to upload existing image
            boolean update =  (args.length > 2 && args[2].startsWith("u"));
            // quiet means only output "#" for each image entry processed instead of full info line
            boolean quiet =  (args.length > 3 && args[3].startsWith("q"));
            System.out.println("Uploading to "+bucketName +" "+folder+" (update:"+update+" quiet:"+quiet+")");
            System.out.println("FTP server to process: "+ftpProps.getProperty("server"));

            // load the "ranFiles" list of .zips that had already been processed in the past
            List<String> ranFiles = new ArrayList<String>();
            File ranFilesFile = new File("ranfiles");
            if (ranFilesFile.exists()) {
                FileReader ranFileRdr = new FileReader(ranFilesFile);
                BufferedReader br = new BufferedReader(ranFileRdr);
                String line;
                while ((line = br.readLine()) != null) {
                    ranFiles.add(line.trim());
                }
            }
            //System.out.println("filesRan:"+ranFiles);

            // connect to the ftp server

            ftp.connect(ftpProps.getProperty("server"));
            System.out.println(ftp.getReplyString());
            // After connection attempt, you should check the reply code to verify
            // success.
            int ftpreply = ftp.getReplyCode();
            if(!FTPReply.isPositiveCompletion(ftpreply)) {
                ftp.disconnect();
                System.err.println("FTP server refused connection.");
                System.exit(1);
            }
            String ftpUsername = ftpProps.getProperty("username");
            String ftpPassword = ftpProps.getProperty("password");
            if (!ftp.login(ftpUsername, ftpPassword))
            {
                ftp.logout();
                ftp.disconnect();
                System.err.println("FTP server refused connection for "+ftpUsername+"/"+ftpPassword);
                System.exit(1);
            }
            System.out.println("Connected to " + ftpProps.getProperty("server"));
            ftp.setListHiddenFiles(false);
            ftp.enterLocalPassiveMode();
            ftp.setUseEPSVwithIPv4(false);

            // get list of all .zip files on the ftp server in specified directory

            String ftpzipDir = ftpProps.getProperty("zipDir");
            FTPFile[] availFiles = ftp.listFiles(ftpzipDir);
            System.out.println("Found "+availFiles.length+" .zip files on the ftp server in "+ftpzipDir);

            // find a file that isn't in the "ranFiles" list

            FTPFile availFile = null;
            String availFileName = null;
            for (FTPFile aFile : availFiles) {
                String name = aFile.getName().substring(aFile.getName().lastIndexOf("/") + 1);
                if (!ranFiles.contains(name)) {
                    availFile = aFile;
                    availFileName = name;
                    break;
                }
                else {
                    //System.out.println("skipping "+name);
                }
            }
            if (availFile == null) {
                System.out.println("No new files found to FTP.");
                ftp.logout();
                ftp.disconnect();
                return;
            }
            else {
                long fileSize = availFile.getSize()/1000000;
                System.out.println("Downloading for processing "+availFile.getName()+" size:"+fileSize+"Mb");
            }


            // download the .zip file from ftp
            long ftpstartTime = System.currentTimeMillis();
            ftp.setCopyStreamListener(createFTPStreamListener(availFile.getSize()));
            File zipFile = new File(availFileName);

            OutputStream output;
            output = new FileOutputStream(zipFile);
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            String remoteName = ftpProps.getProperty("zipDir") + "/" + availFile.getName();
            ftp.retrieveFile(remoteName, output);
            output.close();
            ftp.logout();
            ftp.disconnect();
            if (zipFile.length() == 0) {
                System.out.println("Error downloading "+remoteName);
                return;
            }
            System.out.println("Done downloading in "+((System.currentTimeMillis() - ftpstartTime) / 60000)+" minutes.");


            // now process the zip file and upload its contents to AS3

            ZipFile zFile = new ZipFile(zipFile);
            int numfiles = zFile.size();
            System.out.println(zipFile.getName()+" num entries:"+numfiles);
            long as3startTime = System.currentTimeMillis();
            int i=1;
            Enumeration<? extends ZipEntry> entries = zFile.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (!entry.isDirectory()) {
                    String filename = entry.getName().substring(entry.getName().lastIndexOf("/") + 1);
                    if (!filename.endsWith(".jpg")) {
                        System.out.println("!!Error - file name is not a .jpg!!");
                    }
                    if (!quiet)
                        System.out.print((i++)+" of "+numfiles+" : "+filename + "...");
                    else
                        System.out.print("#");
                    /* check to see if file already exists on AS3 */
                    boolean doupload = true;
                    if (update) {
                        ObjectMetadata exstMD = null;
                        try {
                            exstMD = s3.getObjectMetadata(bucketName, folder+"/"+filename);
                        }
                        catch (Exception ignored) { }
                        if (exstMD != null && exstMD.getContentLength() > 0) {
                            doupload = false;
                            if (!quiet)
                                System.out.println(" - exists - ");
                            else
                                System.out.print("X ");
                        }
                    }
                    if (doupload) {
                        InputStream zfIn = zFile.getInputStream(entry);
                        ObjectMetadata md = new ObjectMetadata();
                        md.setContentLength(entry.getSize());
                        md.setContentType("image/jpeg");

                        int numerrors = 0;
                        boolean completed = false;
                        while (!completed && numerrors < 3) {
                            try {
                                s3.putObject(new PutObjectRequest(bucketName, folder+"/"+filename, zfIn, md));
                                completed = true;
                            }
                            catch (Exception e) {
                                numerrors++;
                                System.out.println("Error on upload "+numerrors);
                                e.printStackTrace();
                                s3 = new AmazonS3Client(new PropertiesCredentials(
                                        FTPProcessor.class.getResourceAsStream("AwsCredentials.properties")), s3conf);
                            }
                        }
                        if (!completed) {
                            throw new RuntimeException("Too many attempts to upload resulted in error");
                        }

                        if (!quiet)
                            System.out.println( "-done." );
                        else
                            System.out.print(" ");
                    }
                }
            }
            if (quiet) System.out.println(" ");
            long elapsedTime = (System.currentTimeMillis() - as3startTime) / 60000;
            System.out.println("Uploaded to AS3 in "+elapsedTime+" minutes.");

            FileWriter lrWr = new FileWriter(ranFilesFile, true);
            PrintWriter lrPWr = new PrintWriter(lrWr);
            lrPWr.println(availFileName);
            lrWr.close();
            System.out.println(availFileName+" added to the ranFiles");

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(ftp.isConnected()) {
                try {
                    ftp.disconnect();
                } catch(IOException ioe) {
                    // do nothing
                }
            }
            System.out.println("Finished FTPProcessor on "+new Date());
            System.out.println("Process took: "+((System.currentTimeMillis()-startTime)/60000)+" minutes.");
        }
    }


    private static CopyStreamListener createFTPStreamListener(final long pFileSize){
        return new CopyStreamListener(){
            private long megsTotal = 0;
            long fileSize = pFileSize;
            public void bytesTransferred(CopyStreamEvent event) {
                bytesTransferred(event.getTotalBytesTransferred(), event.getBytesTransferred(), event.getStreamSize());
            }

            public void bytesTransferred(long totalBytesTransferred,
                                         int bytesTransferred, long streamSize) {
                //System.out.println("totalBytesTransferred:"+totalBytesTransferred);
                //System.out.println("streamSize:"+fileSize);
                long megs = totalBytesTransferred / (1000000*10); // 10Mbs
                long perc = totalBytesTransferred * 100 / fileSize;
                for (long l = megsTotal; l < megs; l++) {
                    System.out.print("#"+perc);
                }
                megsTotal = megs;
            }
        };
    }
}


