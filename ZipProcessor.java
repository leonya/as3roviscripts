import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.*;

/**
 * Scans all .zip files in current directory, unzips them and uploads contents to specified S3 bucket/folder
 * Creates a "ranfiles" file that records names of zip files already processed, and they will not be processed again
 */
public class ZipProcessor {
    public static void main(String[] args) {
        try {
            if (args.length < 2) {
                System.out.println("specify bucket name and folder");
                return;
            }
            ClientConfiguration s3conf = new ClientConfiguration();
            System.out.println("timeout:"+s3conf.getSocketTimeout());
            s3conf.setSocketTimeout(100000);
            System.out.println("new timeout:"+s3conf.getSocketTimeout());
            AmazonS3 s3 = new AmazonS3Client(new PropertiesCredentials(
                    ZipProcessor.class.getResourceAsStream("AwsCredentials.properties")), s3conf);
            int numerrors = 0;

            List<String> ranFiles = new ArrayList<String>();
            File ranFilesFile = new File("ranfiles");
            if (ranFilesFile.exists()) {
                FileReader ranFileRdr = new FileReader(ranFilesFile);
                BufferedReader br = new BufferedReader(ranFileRdr);
                String line;
                while ((line = br.readLine()) != null) {
                    ranFiles.add(line);
                }
            }
            System.out.println("filesRan:"+ranFiles);
            String bucketName = args[0];
            String folder = (args.length > 1) ? args[1] : "";
            boolean quiet =  (args.length > 2 && args[2].startsWith("q"));
            System.out.println("Uploading to "+bucketName +" "+folder);

            File dir = new File(".");
            File[] zipFiles = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File file, String s) {
                    return (s.endsWith(".zip"));
                }
            });
            for (File zipFile : zipFiles) {
                if (!ranFiles.contains(zipFile.getName())) {
                    ZipFile zFile = new ZipFile(zipFile);
                    int numfiles = zFile.size();
                    System.out.println(zipFile.getName()+" num entries:"+numfiles);
                    long startTime = System.currentTimeMillis();
                    int i=1;
                    Enumeration<? extends ZipEntry> entries = zFile.entries();
                    while (entries.hasMoreElements()) {
                        ZipEntry entry = entries.nextElement();
                        if (!entry.isDirectory()) {
                            String filename = entry.getName().substring(entry.getName().lastIndexOf("/") + 1);
                            if (!filename.endsWith(".jpg")) {
                                System.out.println("!!Error - file name is not a .jpg!!");
                            }
                            if (!quiet)
                                System.out.print((i++)+" of "+numfiles+" : "+filename + "...");
                            else
                                System.out.print("#");
                            ObjectMetadata exstMD = null;
                            try {
                                exstMD = s3.getObjectMetadata(bucketName, folder+"/"+filename);
                            }
                            catch (Exception ignored) { }
                            if (exstMD != null && exstMD.getContentLength() > 0) {
                                if (!quiet)
                                    System.out.println(" - exists - ");
                                else
                                    System.out.print("X ");
                            }
                            else {
                                InputStream zfIn = zFile.getInputStream(entry);
                                ObjectMetadata md = new ObjectMetadata();
                                md.setContentLength(entry.getSize());
                                md.setContentType("image/jpeg");
                                boolean completed = false;
                                while (!completed && numerrors < 3) {
                                    try {
                                        s3.putObject(new PutObjectRequest(bucketName, folder+"/"+filename, zfIn, md));
                                        completed = true;
                                    }
                                    catch (Exception e) {
                                        numerrors++;
                                        System.out.println("Error on upload "+numerrors);
                                        e.printStackTrace();
                                        s3 = new AmazonS3Client(new PropertiesCredentials(
                                                ZipProcessor.class.getResourceAsStream("AwsCredentials.properties")), s3conf);
                                    }
                                }
                                if (!completed) {
                                    throw new RuntimeException("Too many attempts to upload resulted in error");
                                }
                                numerrors = 0;
                                if (!quiet)
                                    System.out.println( "-done." );
                                else
                                    System.out.print(" ");
                            }
                        }
                    }
                    if (quiet) System.out.println(" ");
                    long elapsedTime = (System.currentTimeMillis() - startTime) / 1000;
                    System.out.println("Finished in "+elapsedTime+" sec.");

                    FileWriter lrWr = new FileWriter(ranFilesFile, true);
                    PrintWriter lrPWr = new PrintWriter(lrWr);
                    lrPWr.println(zipFile.getName());
                    lrWr.close();
                }
                else {
                    System.out.println(zipFile.getName()+" already written");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
